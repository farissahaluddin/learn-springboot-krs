package com.faris.heyspringboot;

import com.faris.heyspringboot.entity.MataKuliah;
import com.faris.heyspringboot.service.MataKuliahService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HeyspringbootApplicationTests {

    @Autowired
    private MataKuliahService mataKuliahService;

    @Test
    public void contextLoads() {

        for (MataKuliah l : mataKuliahService.findAll())
        {
            System.out.println(l.getMataKuliah());
        }

    }



}
