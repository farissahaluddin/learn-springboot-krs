package com.faris.heyspringboot.dao;

import com.faris.heyspringboot.entity.MataKuliah;

import java.util.List;

public interface MataKuliahDAO extends BaseDAO<MataKuliah> {
    List<MataKuliah> findByMataKuliah(MataKuliah param);

}
