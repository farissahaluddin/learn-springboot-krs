package com.faris.heyspringboot.dao;

import com.faris.heyspringboot.entity.User;

import java.util.List;

public interface UserDAO extends BaseDAO<User> {

    List<User> findByUsername(User param);

}
